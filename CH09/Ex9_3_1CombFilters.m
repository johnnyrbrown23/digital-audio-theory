%% Programming Example 9.3.1 - Comb Filters
% DigitalAudioTheory.com

ak=1.0;	% gain on the delay tap
k=1;		% amount of delay on the delay tap
 
Fs=48000;	% sample rate = 48 khz
f=[0:23999];% frequency array from DC to Nyquist
 
% magnitude response
Hmag=sqrt(1+2*ak*cos(2*pi*f*k/Fs)+ak^2);

% plotting and labeling axes 
plot(f,20*log10(Hmag))
box on; grid on; 
xlabel('Freq (Hz)'); 
ylabel('Mag (dB)'); 
title('Comb Filter'); 
xlim([f(1) f(end)])

 
%%
clf;
for k=1:5
    Hmag=sqrt(1+2*ak*cos(2*pi*f*k/Fs)+ak^2);
    
    plot(f, 20*log10(Hmag));
    hold on;
    box on; grid on;
    
end
 
title('Comb Filter - k sweep')
ylabel('Gain (dB)'); 
xlabel('Freq (Hz)'); 
box on; grid on; 
axis([f(1) f(end) -40 12])

%%
clf;
k=4;
 
for ak=-1:0.5:1
    Hmag=sqrt(1+2*ak*cos(2*pi*f*k/Fs)+ak^2);
    hold on;
    plot(f, 20*log10(Hmag));
end
 
title('Comb Filter - a sweep')
ylabel('Gain (dB)'); 
xlabel('Freq (Hz)'); 
box on; grid on; 
axis([f(1) f(end) -40 12])

