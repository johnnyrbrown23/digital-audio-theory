N=1000;
even=zeros(N,1);
odd=even;
actual_noise=even;
x=sin(linspace(0,4*pi,N))';
for ii=1:256
    n = randn(N,1);
    actual_noise = actual_noise+n;
    
    if (mod(ii,2))
        even = even+n+x;
    else
        odd=odd+n+x;
    end
end

even_avg = even/(ii/2);
odd_avg = odd/(ii/2);
act_avg = actual_noise/ii;

db(rms(act_avg))
db(rms((even_avg-odd_avg)/2))

