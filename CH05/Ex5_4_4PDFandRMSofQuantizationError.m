%% Programming Example 5.4.4 - PDF and RMS of quantization error
% DigitalAudioTheory.com

% draw the quantization error pdf
n  = 2*rand(10000000,1)-1.0;
[n8,q] = quantBits(n,8,1);
eq_n8 = n - n8;


% for Matlab use:
histogram(eq_n8, linspace(-q/2, q/2, 50), 'Normalization', 'pdf');
% for Octave use:
%hist(eq_n8, linspace(-q/2, q/2, 50), "Normalization", "pdf");
grid on;


eq_rms = q/sqrt(12)	% estimate of eq RMS
sqrt(mean(eq_n8.^2))	% actual eq RMS

