%% Programming Example 5.5.2 - �-law quantization
% DigitalAudioTheory.com

u=255;	% mu-law compression factor
[y24, fs]=audioread('flute.wav', [1 11*44100]);	% 11s of flute solo
 
% linear quantizer
y8 = quantBits(y24,8,1);
 
% mu-law quantizer
y24mu = sign(y24).*log(1+u*abs(y24))/log(1+u);
y8mu_comp = quantBits(y24mu,8,1);
y8mu = sign(y8mu_comp)*(1/u).*((1+u).^(abs(y8mu_comp))-1);

sound([y24; y8; y8mu],fs)

plot(y8mu(:,1));
hold on;
plot(y8(:,1));

