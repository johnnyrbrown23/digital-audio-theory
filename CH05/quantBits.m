function [Q, q] = quantBits(input, N, A)

% Q = quantBits(input, N, A)
% 
% This functions quantizes the input according to the bit depth of N for a
% signal with dynamic range A

% find quantization step size
q=(A-(-A))/(2^N);

% define value overrun (clip)
input(input>A-q) = A-q;
input(input<-A) = -A; 

Q = round(input/q)*q;
