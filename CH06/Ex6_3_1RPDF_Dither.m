%% Programming Example 6.3.1 - RPDF Dither
% DigitalAudioTheory.com

N=8;            % bit depth
A=1;            % max amplitude
fs=40000;       % sample rate (Hz)
f=40;           % frequency (Hz)
T=0.05;         % duration (s)
 
x = 0.95*cos(2*pi*f*[1:T*fs]/fs);
x8 = quantBits(x,N,A);
 
plot(x8);
grid on; 


q=2*A/2^N;
% random noise in the range of +/-q/2
d=q*(rand(1,T*fs)-0.5);     

% add to the signal *before* quantizing
x8d = quantBits(x+d,N,A);   

hold on; plot(x8d)


20*log10(2^0.5)
20*log10(rms(x8d-x)/rms(x8-x))

% for Matlab use:
histogram(d, linspace(-q/2, q/2, 50), 'Normalization', 'pdf');
% for Octave use:
%hist(d, linspace(-q/2, q/2, 50));


t=[1:T*fs]/fs;
x = (q/4)*cos(2*pi*f*t);
 
% note: the 8-bit encoder *can't* capture the 0.25-bit signal

% add "rectangular" dither
x8d = quantBits(x+d,N,A);
 
plot(t,x); hold on;
ylim([-2*q 2*q]); grid on;
plot(t,x8d); grid on;

