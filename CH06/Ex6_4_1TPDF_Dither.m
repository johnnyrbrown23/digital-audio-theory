%% Programming Example 6.4.1 - TPDF Dither
% DigitalAudioTheory.com

d_tri = zeros(size(d));
d_tri(1) = d(1);	% the first value of d_tri is the same as d

% each subsequent sample is a difference with the preceding value
d_tri(2:end) = d(2:end)-d(1:end-1);


x8d_tri = quantBits(x+d_tri,N,A);
plot(t, x8d_tri);
hold on; box on;
plot(t, x)


figure();
% for Matlab use:
histogram(d_tri, linspace(-q, q, 50), 'Normalization', 'pdf')
% for Octave use:
hist(d_tri, linspace(-q, q, 50));
xlim([-q q])

