%% Programming Example 8.4.2 - Matched Filtering
% DigitalAudioTheory.com

x=audioread('drum.wav');
s=audioread('snare_hit.wav');
 
[similarity,lag] = xcorr(x,s);
 
I = find(similarity>50);
 
plot(x)
hold on;
stem(lag(I), ones(size(I)))
