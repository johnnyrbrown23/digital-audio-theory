%% Programming Example 12.4.1 - DFT of a Sinusoid
% DigitalAudioTheory.com

N=32;
r=4;    % 6 kHz @ fS=48k
W=exp(-j*2*pi/N); % twiddle factor

cos_r = 0.5*W.^(r*[0:N-1]) + 0.5*W.^(-r*[0:N-1]);
plot(real(cos_r))
 
for k=0:N-1 
    
    X(k+1)=0;
    for n=0:N-1
        X(k+1)=X(k+1)+0.5*W^((k-r)*n)+0.5*W^((k+r)*n);
    end
    disp(['|X(k=' num2str(k) ')| = ' num2str(abs(X(k+1)))]);
    
end
 
figure; plot(0:N-1,(abs(X)))

