%% Programming Example 12.7.1 - Frequency Interpolation
% DigitalAudioTheory.com

N=1024;
n=0:N-1;
fs=44100;
k=24;
 
x1=sin(2*pi*(k/N*fs)*n/fs);
x2=sin(2*pi*1000*n/fs);
 
X1=fft(x1, N);
X2=fft(x2, N);
 
f=linspace(0,fs,N);
 
stem(f,abs(X1));
hold on;
stem(f,abs(X2));

yp1=abs(X2(k+1));
ym1=abs(X2(k-1));
d = (yp1 - ym1)/(yp1+ym1);
f_est = (k+d-1)*fs/N 

