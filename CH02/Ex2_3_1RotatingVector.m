%% Programming Example 2.3.1 - Rotating Vectors
% DigitalAudioTheory.com

a=0.5;          % magnitude
t_step=1/12;    % sec
t_max=1;      % sec
f = 1;          % Hz
vv = [];	    % will hold array of vectors (Octave)
for t = 0:t_step:t_max
    theta = 2*pi*f*t;
    v = a*exp(j*theta);
    vv = [vv v];	 % concatenate array of vectors (Octave)
    gs = 1-t/t_max;  % RGB triplet value
    % polarplot is Matlab only, 
    % for Octave, comment this next line and use the 
    % polar() function *outside* the for-loop 
    polarplot(v, 'Marker', 'o', 'MarkerSize', 20, 'MarkerFaceColor', [gs gs gs], 'MarkerEdgeColor', 'k');
    hold on;
    pause(t_step); drawnow;
end

pax = gca;					% Matlab only
pax.ThetaAxisUnits = 'radians';	% Matlab only

% use this for Octave only
%polar(0:t_step*2*pi:t_max*2*pi, abs(vv), 'o') 

