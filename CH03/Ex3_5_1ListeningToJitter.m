%% Programming Example 3.5.1 - Listening to Jitter
% DigitalAudioTheory.com
clear

% sample rate of 44.1 kHz
Fs=44100;

% sample rate conversion factor
src = 100;

% create the 1 kHz sinusoid
x=sin(2*pi*1000*(0:Fs*src)/(Fs*src));

% set the std. dev. of the jitter in nano-sec 
j_ns = 1000;

% convert from ns to samples
j_rms = j_ns*1e-9*Fs*src;

% sample index for 44.1 kHz
cnt=1;

% iterate over the entire digital signal, in skips of src
for n=1:src:length(x)

    % generate a random number
    jit = randn();

    % 1) scale the jitter amount by j_rms
    % 2) convert to an integer 
    % 3) make sure index is in-bounds
    idx = max(1, round(n+j_rms*jit));
    idx = min(idx, length(x));

    % capture the value of x at jittered intervals
    y(cnt) = x(idx);

    % increment the sample index (44.1 kHz)
    cnt=cnt+1;
end
 
sound(y,Fs)

