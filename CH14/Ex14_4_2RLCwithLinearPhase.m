%% Programming Example 14.4.2 - RLC with Linear Phase
% DigitalAudioTheory.com

% digital parameters
Fs = 44100;             % sample rate
N = 1024;               % length of FFT 

% analog prototype
R = 10; 
L = 2e-3; 
C = 5e-6;

f=0:Fs/N:Fs;            % <- 0 to Fs
Hc = freqs([1/(R*C) 0],[1 1/(R*C) 1/(L*C)], 2*pi*f);

% digital model
ph=linspace(0,-N/4,N/2+1); % line from 0 (@ 0 Hz) to -N/4 (@ fN Hz) 
Hbb = abs(Hc(1:N/2+1)).*exp(j*2*pi*ph); % mag*e^(j*2*pi*ph) 


Hconj = conj(Hbb(2:end-1));     % take conjugate (excl fN and DC)
Hm = [Hbb fliplr(Hconj)];       % mirror about Nyquist (see Fig. 14-6)

ir = real(ifft(Hm));            % impulse response


F=f(1:N/2+1);
H=freqz(ir,1,2*pi*F/Fs);% actual frequency response of IR
 
% plot
semilogx(F, 20*log10(abs(Hc(1:N/2+1)))); hold on;
semilogx(F, 20*log10(abs(H(1:N/2+1))), '.'); grid on;
figure; plot(ir)

