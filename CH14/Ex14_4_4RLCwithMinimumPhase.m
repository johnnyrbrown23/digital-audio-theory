%% Programming Example 14.4.4 - RLC with Minimum Phase
% DigitalAudioTheory.com

% analog prototype
R = 10; 
L = 2e-3; 
C = 5e-6;

f=linspace(-Fs/2,Fs/2,N); % Hilbert() wants DC in middle
Hc = freqs([1/(R*C) 0],[1 1/(R*C) 1/(L*C)], 2*pi*f);


min_ph = unwrap(-imag(hilbert(log(abs(Hc)))));


Hm = abs(Hc).*exp(j*min_ph); % complex freq resp (base band)
 
% re-structure Hm to go from 0 to Fs, rather than -Fs/2 to Fs/2
Hm=[Hm(end/2:end) Hm(1:end/2-1)];
 
ir = real(ifft(Hm));        % impulse response


F=f(N/2+1:end);
 
H=freqz(ir,1, 2*pi*F/Fs);     % actual frequency response of IR
 
semilogx(F, 20*log10(abs(Hc(N/2+1:end)))); hold on;
semilogx(F, 20*log10(abs(H(1:N/2))), 'o'); grid on;
figure; plot(ir)

