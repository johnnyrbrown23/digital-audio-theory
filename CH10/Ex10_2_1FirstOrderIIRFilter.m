%% Programming Example 10.2.1 - First Order IIR Filter
% DigitalAudioTheory.com

% 1st order IIR filter magnitude
a0=0.9;
a1=0.9;
b1=0.1;
fs=48000; 
 
% method 1
f=0:1:fs/2;     % freq array
w=2*pi*f/fs;    % omega array
H1 = (a0*exp(j*w)+a1)./(exp(j*w)+b1);
plot(f,20*log10(abs(H1)));
grid on; xlabel('freq'); ylabel('gain (dB)')
 
% method 2
num=[a0, a1];
den=[1, b1];  

[H1,F]=freqz(num,den, fs, fs);
subplot(3,1,1)
plot(F,20*log10(abs(H1))); grid on;
subplot(3,1,2)
plot(F, angle(H1)); grid on;
 
% and group delay
[Grd,~]=grpdelay(num,den, fs, fs);
subplot(3,1,3); 
plot(F, Grd); grid on
 
% p/z plot
figure;
zplane(num,den)

